export default function handleDrop(event) {
    const { files } = event.dataTransfer;
    return files[0]
}