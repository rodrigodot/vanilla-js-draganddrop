import previewFile from "./previewFile.js";
import handleDrop from "./handleDrop.js";
import handleFiles from "./handleFiles.js";

class DragAndDrop {
    constructor(el) {
        this.el = el;
    }

    init() {
        const dropArea = this.el.querySelector("#drop-area");
        const fileElem = this.el.querySelector("#fileElem");
        const gallery = this.el.querySelector("#gallery");

        const preventDefaults = (e) => {
            e.preventDefault();
            e.stopPropagation();
        };

        const activated = () => {
            dropArea.classList.add("highlight");
        };

        const disabled = () => {
            dropArea.classList.remove("highlight");
        };

        dropArea.addEventListener("drop", (event) => {
            const file = handleDrop(event);
            previewFile(file, gallery);
        });

        fileElem.addEventListener("change", () => {
            handleFiles(fileElem.files[0]);
            previewFile(fileElem.files[0], gallery);
        });

        ["dragenter", "dragover", "dragleave", "drop"].forEach((eventName) => {
            dropArea.addEventListener(eventName, preventDefaults, false);
        });

        ["dragenter", "dragover"].forEach((eventName) => {
            dropArea.addEventListener(eventName, activated, false);
        });

        ["dragleave", "drop"].forEach((eventName) => {
            dropArea.addEventListener(eventName, disabled, false);
        });
    }

    run() {
        this.init();
    }
}

export { DragAndDrop };