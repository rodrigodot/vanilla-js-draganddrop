export default function previewFile(file, gallery) {

    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onloadend = () => {
        gallery.innerHTML = ''
        if (
            file.type === "image/jpeg" ||
            file.type === "image/png" ||
            file.type === "image/gif"
        ) {
            const img = document.createElement("img");
            img.src = reader.result;
            gallery.appendChild(img);
        } else {
            const doc = document.createElement("img");
            doc.src = "assets/img/document.png";
            gallery.appendChild(doc);
        }
    };
}