import { DragAndDrop } from "./components/drag-and-drop/drag-and-drop.js";

const element = document.querySelector("#drag-and-drop");
const dragAndDrop = new DragAndDrop(element);
dragAndDrop.run();