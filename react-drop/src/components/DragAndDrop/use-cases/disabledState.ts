const disabled = (targetElement: HTMLElement, className = "highlight") => {
  targetElement.classList.remove(className);
};

export default disabled;
