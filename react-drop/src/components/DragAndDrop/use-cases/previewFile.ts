export default function previewFile(file: File, gallery: HTMLElement) {
  let reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onloadend = () => {
    gallery.innerHTML = "";
    if (
      file.type === "image/jpeg" ||
      file.type === "image/png" ||
      file.type === "image/gif"
    ) {
      const img = document.createElement("img");
      img.src = reader.result as string;
      gallery.appendChild(img);
    } else {
      const doc = document.createElement("img");
      doc.src = `${process.env.PUBLIC_URL}/assets/img/document.png`;
      gallery.appendChild(doc);
    }
  };
}
