const activated = (targetElement: HTMLElement, className = "highlight") => {
  targetElement.classList.add(className);
};

export default activated;
