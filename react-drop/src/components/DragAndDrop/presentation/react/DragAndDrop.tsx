import React from "react";
import { DragAndDrop } from "../../domain/drag-and-drop";

import "../drag-and-drop.css";

const DragNDrop = () => {
  React.useEffect(() => {
    const dragAndDrop = new DragAndDrop(
      "#drag-and-drop",
      "#gallery",
      "#drop-area",
      "#fileElem"
    );
    dragAndDrop.run();
  });

  return (
    <div id="drag-and-drop">
      <div id="drop-area">
        <form className="my-form">
          <p>Upload a file by dragging and dropping</p>
          <input type="file" id="fileElem" multiple accept="file_extension" />
          <label className="button" htmlFor="fileElem">
            Upload File
          </label>
        </form>
        <div id="gallery" />
      </div>
    </div>
  );
};

export default DragNDrop;
