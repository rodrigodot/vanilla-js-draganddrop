import {
  handleDrop,
  previewFile,
  handleFile,
  uploadFile,
  activated,
  disabled,
} from "../use-cases";

class DragAndDrop {
  dropArea: HTMLElement;
  fileElem: HTMLInputElement;
  gallery: HTMLElement;

  constructor(
    private elementRoot: string,
    private galleryRoot: string,
    private dropAreaRoot: string,
    private inputElementRoot: string
  ) {
    const root = document.querySelector(this.elementRoot) as HTMLElement;
    this.dropArea = root.querySelector(this.dropAreaRoot) as HTMLElement;
    this.fileElem = root.querySelector(
      this.inputElementRoot
    ) as HTMLInputElement;
    this.gallery = root.querySelector(this.galleryRoot) as HTMLElement;
  }

  init() {
    const preventDefaults = (e: {
      preventDefault: () => void;
      stopPropagation: () => void;
    }) => {
      e.preventDefault();
      e.stopPropagation();
    };

    this.dropArea.addEventListener("drop", (event: DragEvent) => {
      const files = event.dataTransfer?.files;

      if (files?.length) {
        const file = handleDrop(files[0]);
        previewFile(file, this.gallery);
        uploadFile(file);
      }
    });

    this.fileElem.addEventListener("change", (event: Event) => {
      const { files } = event.target as HTMLInputElement;

      if (files?.length) {
        handleFile(files[0]);
        uploadFile(files[0]);
        previewFile(files[0], this.gallery);
      }
    });

    ["dragenter", "dragover", "dragleave", "drop"].forEach((eventName) => {
      this.dropArea.addEventListener(eventName, preventDefaults, false);
    });

    ["dragenter", "dragover"].forEach((eventName) => {
      this.dropArea.addEventListener(
        eventName,
        () => activated(this.dropArea),
        false
      );
    });

    ["dragleave", "drop"].forEach((eventName) => {
      this.dropArea.addEventListener(
        eventName,
        () => disabled(this.dropArea),
        false
      );
    });
  }

  run() {
    this.init();
  }
}

export { DragAndDrop };
